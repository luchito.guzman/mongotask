import { Controller, Get, Post, Put, Delete, Body, Param} from '@nestjs/common';

import { CreateTaskDto } from './dto/create-task.dto';
import { Task } from './interface/Task';
import { TasksService } from './tasks.service';


@Controller('tasks')
export class TasksController {

    constructor(private taskService:TasksService){};

    @Get()
    getTasks(){
        return this.taskService.getTasks();
    }

    @Get(':taskId')
    getTask(@Param('taskId') taskId:string){
        return this.taskService.getTask(taskId);
    }

    @Post()
    createTask(@Body() task: CreateTaskDto): Promise<Task>{
        return this.taskService.createTask(task)
    }

    @Put(':id')
    updateTask(@Body() task:CreateTaskDto, @Param('id') id):string{
        console.log(task);
        console.log(id);
        return 'Actualizando'
    }

    @Delete(':id')
    deleteTask(@Param('id') id):string{
        console.log(id);
        return 'Eliminando Tarea : ' + id;
    }
}
